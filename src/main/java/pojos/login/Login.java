package pojos.login;

/*******************************************************************************
 * Autor    : felipe bessa
 * ID       : 727109
 * Email    : felipe.bessa@yaman.com.br
 * Data     : 23/04/2018
 * Empresa  : Yaman / Redecard
 *******************************************************************************/

public class Login {

    private String username;
    private String credential;
    private String partnerCtrlId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCredential() {
        return credential;
    }

    public void setCredential(String credential) {
        this.credential = credential;
    }

    public String getPartnerCtrlId() {
        return partnerCtrlId;
    }

    public void setPartnerCtrlId(String partnerCtrlId) {
        this.partnerCtrlId = partnerCtrlId;
    }
}