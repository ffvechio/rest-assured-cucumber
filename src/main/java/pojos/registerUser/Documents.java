package pojos.registerUser;

/*******************************************************************************
 * Autor    : felipe bessa
 * ID       : 727109
 * Email    : felipe.bessa@yaman.com.br
 * Data     : 23/04/2018
 * Empresa  : Yaman / Redecard
 *******************************************************************************/

public class Documents {

    private String CPF;

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

}
