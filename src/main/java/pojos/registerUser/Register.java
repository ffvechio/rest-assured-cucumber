package pojos.registerUser;

/*******************************************************************************
 * Autor    : felipe bessa
 * ID       : 727109
 * Email    : felipe.bessa@yaman.com.br
 * Data     : 23/04/2018
 * Empresa  : Yaman / Redecard
 *******************************************************************************/

public class Register {

    private String name;
    private String email;
    private Documents documents;
    private Phone phones;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Documents getDocuments() {
        return documents;
    }

    public void setDocuments(Documents documents) {
        this.documents = documents;
    }

    public Phone getPhones() {
        return phones;
    }

    public void setPhones(Phone phones) {
        this.phones = phones;
    }

}