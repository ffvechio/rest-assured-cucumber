package pojos.registerUser;

/*******************************************************************************
 * Autor    : felipe bessa
 * ID       : 727109
 * Email    : felipe.bessa@yaman.com.br
 * Data     : 23/04/2018
 * Empresa  : Yaman / Redecard
 *******************************************************************************/

public class Phone {

    private String mobile;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
