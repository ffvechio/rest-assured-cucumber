package pojos.registerUser;

/*******************************************************************************
 * Autor    : felipe bessa
 * ID       : 727109
 * Email    : felipe.bessa@yaman.com.br
 * Data     : 23/04/2018
 * Empresa  : Yaman / Redecard
 *******************************************************************************/

public class Password {

    private String credential;
    private String createdToken;


    public String getCredential() {
        return credential;
    }

    public void setCredential(String credential) {
        this.credential = credential;
    }

    public String getCreatedToken() {
        return createdToken;
    }

    public void setCreatedToken(String createdToken) {
        this.createdToken = createdToken;
    }
}