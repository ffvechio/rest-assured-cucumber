package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        monochrome = true,
        plugin = {"html:Reports/cucumber/cucumber-html-report",
                "json:Reports/cucumber/cucumber.json", "pretty:Reports/cucumber/cucumber-pretty.txt",
                "testng:Reports/cucumber/cucumber-results.xml"},
        glue = "stepdefinition",
        features = {"features/Customers_01.feature"},
        tags={"@teste01"})
public class RunnerTest extends AbstractTestNGCucumberTests {
}
