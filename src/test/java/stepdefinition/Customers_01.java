package stepdefinition;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojos.registerUser.Documents;
import pojos.registerUser.Phone;
import pojos.registerUser.Register;
import utils.PropertiesHelper;

import java.util.Properties;

public class Customers_01 {
    private Properties properties = new PropertiesHelper().getProperties();
    public Register register = new Register();
    public Documents documentsCPF = new Documents();
    public Phone phone = new Phone();
    private String jsonRequest = null;
    public String createdToken;

    private Response response;
    private static RequestSpecification request = RestAssured.with();

    @Dado("^que o cliente informa seus dados cadastrais \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void que_o_cliente_informa_seus_dados_cadastrais(String pName, String pCpf, String pTelefone, String pEmail) throws Throwable {

        documentsCPF.setCPF(pCpf);
        phone.setMobile(pTelefone);

        register.setName(pName);
        register.setDocuments(documentsCPF);
        register.setPhones(phone);
        register.setEmail(pEmail);

        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).setSerializationInclusion(JsonInclude.Include.NON_NULL);

        try {
            jsonRequest = mapper.writeValueAsString(register);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        request.given()
                .contentType(ContentType.JSON)
                .body(jsonRequest);

    }

    @Quando("^faço uma requisição POST para \"([^\"]*)\"$")
    public void faço_uma_requisição_POST_para(String pEndpoint) throws Throwable {

        response = request.when().post(pEndpoint);

    }

    @Então("^o código de resposta é (\\d+)$")
    public void o_código_de_resposta_é(int pStatuscode) throws Throwable {

        createdToken = response.then().statusCode(pStatuscode).extract().path("createdToken").toString();;

        System.out.println(createdToken);

    }

}
