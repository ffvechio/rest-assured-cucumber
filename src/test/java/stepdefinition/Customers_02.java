package stepdefinition;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojos.registerUser.Documents;
import pojos.registerUser.Phone;
import pojos.registerUser.Register;
import utils.PropertiesHelper;

import java.util.Properties;

public class Customers_02 {
    private Properties properties = new PropertiesHelper().getProperties();
    public Register register = new Register();
    public Documents documentsCPF = new Documents();
    public Phone phone = new Phone();
    private String jsonRequest = null;
    public String createdToken;

    private Response response;
    private static RequestSpecification request = RestAssured.with();

    @Dado("^que estou no ambiente da API customers e preencho o campo name \"([^\"]*)\"$")
    public void que_estou_no_ambiente_da_API_customers_e_preencho_o_campo_name(String pName) throws Throwable {

        register.setName(pName);

    }


    @Dado("^preencho o campo cpf \"([^\"]*)\"$")
    public void preencho_o_campo_cpf(String pCpf) throws Throwable {

        documentsCPF.setCPF(pCpf);
        register.setDocuments(documentsCPF);
    }

    @Dado("^preencho o campo mobile \"([^\"]*)\"$")
    public void preencho_o_campo_mobile(String pMobile) throws Throwable {

        phone.setMobile(pMobile);
        register.setPhones(phone);


    }

    @Dado("^preencho o campo email \"([^\"]*)\"$")
    public void preencho_o_campo_email(String pEmail) throws Throwable {

        register.setEmail(pEmail);

        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).setSerializationInclusion(JsonInclude.Include.NON_NULL);

        try {
            jsonRequest = mapper.writeValueAsString(register);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        request.given()
                .contentType(ContentType.JSON)
                .body(jsonRequest);

    }

    @Quando("^faço uma requisição POST para o endpoint \"([^\"]*)\"$")
    public void faço_uma_requisição_POST_para_o_endpoint(String pEndpoint) throws Throwable {

        response = request.when().post(pEndpoint);

    }

    @Então("^o código de resposta (\\d+)$")
    public void o_código_de_resposta(int pStatuscode) throws Throwable {

        createdToken = response.then().statusCode(pStatuscode).extract().path("createdToken").toString();;

        System.out.println(createdToken);

    }


}
