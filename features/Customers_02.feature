# language: pt
# encoding UTF-8
Funcionalidade: Preencher formulário de cadastro

  Como um novo usuário
  Eu quero enviar meus dados cadastraias para o endpoit /customer
  De modo que enviando os dados eu possa me cadastrar


  @teste02
  Esquema do Cenario: Enviar dados cadastrais para o endpoint /customers.
    Dado que estou no ambiente da API customers e preencho o campo name <name>
    E preencho o campo cpf <cpf>
    E preencho o campo mobile <mobile>
    E preencho o campo email <email>
    Quando faço uma requisição POST para o endpoint <endpoint>
    Então o código de resposta <statuscode>

    Exemplos:
      | name                 | cpf               | mobile                 | email                | endpoint                                                         | statuscode     |
      | "Nome de Teste"      | "60893884324"     | "11912345678"          | "mail1@mail1.com"    | "https://api.useredepay.com.br/api-integracao/customers"         | 201            |
      | "Nome de Testee"     | "90952988259"     | "11912345678"          | "mail2@mail2.com"    | "https://api.useredepay.com.br/api-integracao/customers"         | 201            |