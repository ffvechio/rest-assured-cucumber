# language: pt
# encoding UTF-8
Funcionalidade: Preencher formulário de cadastro

  Como um novo usuário
  Eu quero enviar meus dados cadastraias para o endpoit /customer
  De modo que enviando os dados eu possa me cadastrar

  @teste01
  Esquema do Cenario: Enviar dados cadastrais para o endpoint /api-integracao/customers.
    Dado que o cliente informa seus dados cadastrais <name> <cpf> <telefone> <email>
    Quando faço uma requisição POST para "https://api.useredepay.com.br/api-integracao/customers"
    Então o código de resposta é <statuscode>

    Exemplos:
      | name                 | cpf               | telefone               | email                | statuscode      |
      | "Nome de Teste"      | "60893884324"     | "11912345678"          | "mail1@mail1.com"    | 201             |
#     | "Nome de Testee"     | "90952988259"     | "11912345678"          | "mail2@mail2.com"    | 201             |